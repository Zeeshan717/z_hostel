// To parse this JSON data, do
//
//     final candidatePaymentModel = candidatePaymentModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

CandidatePaymentModel candidatePaymentModelFromJson(String str) => CandidatePaymentModel.fromJson(json.decode(str));

String candidatePaymentModelToJson(CandidatePaymentModel data) => json.encode(data.toJson());

class CandidatePaymentModel {
  CandidatePaymentModel({
    required this.payDetail,
    required this.success,
    required this.message,
  });

  List<PayDetail> payDetail;
  String success;
  String message;

  factory CandidatePaymentModel.fromJson(Map<String, dynamic> json) => CandidatePaymentModel(
    payDetail: List<PayDetail>.from(json["pay_detail"].map((x) => PayDetail.fromJson(x))),
    success: json["success"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "pay_detail": List<dynamic>.from(payDetail.map((x) => x.toJson())),
    "success": success,
    "message": message,
  };
}

class PayDetail {
  PayDetail({
    required this.paymentTxnId,
    required this.transactionDate,
    required this.transactionTime,
    required this.status,
    required this.payTransactionId,
    required this.paymentAmount,
    required this.refundStatus,
    required this.feesAmount,
    required this.refundTxnId,
    required this.refundDate,
    required this.refundAmount,
    required this.url,
  });

  String paymentTxnId;
  DateTime transactionDate;
  String transactionTime;
  String status;
  String payTransactionId;
  String paymentAmount;
  String refundStatus;
  String feesAmount;
  String refundTxnId;
  String refundDate;
  String refundAmount;
  String url;

  factory PayDetail.fromJson(Map<String, dynamic> json) => PayDetail(
    paymentTxnId: json["payment_txn_id"],
    transactionDate: DateTime.parse(json["transaction_date"]),
    transactionTime: json["transaction_time"],
    status: json["status"],
    payTransactionId: json["pay_transaction_id"],
    paymentAmount: json["payment_amount"],
    refundStatus: json["refund_status"],
    feesAmount: json["fees_amount"],
    refundTxnId: json["refund_txn_id"],
    refundDate: json["refund_date"],
    refundAmount: json["refund_amount"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "payment_txn_id": paymentTxnId,
    "transaction_date": "${transactionDate.year.toString().padLeft(4, '0')}-${transactionDate.month.toString().padLeft(2, '0')}-${transactionDate.day.toString().padLeft(2, '0')}",
    "transaction_time": transactionTime,
    "status": status,
    "pay_transaction_id": payTransactionId,
    "payment_amount": paymentAmount,
    "refund_status": refundStatus,
    "fees_amount": feesAmount,
    "refund_txn_id": refundTxnId,
    "refund_date": refundDate,
    "refund_amount": refundAmount,
    "url": url,
  };
}
