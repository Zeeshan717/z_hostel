// To parse this JSON data, do
//
//     final bankDetailsModel = bankDetailsModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

BankDetailsModel bankDetailsModelFromJson(String str) => BankDetailsModel.fromJson(json.decode(str));

String bankDetailsModelToJson(BankDetailsModel data) => json.encode(data.toJson());

class BankDetailsModel {
  BankDetailsModel({
    required this.bankDetail,
    required this.success,
    required this.message,
  });

  List<BankDetail> bankDetail;
  String success;
  String message;

  factory BankDetailsModel.fromJson(Map<String, dynamic> json) => BankDetailsModel(
    bankDetail: List<BankDetail>.from(json["bank_detail"].map((x) => BankDetail.fromJson(x))),
    success: json["success"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "bank_detail": List<dynamic>.from(bankDetail.map((x) => x.toJson())),
    "success": success,
    "message": message,
  };
}

class BankDetail {
  BankDetail({
    required this.bankMasterId,
    required this.bankName,
    required this.accountNo,
    required this.branchName,
    required this.ifscNo,
    required this.accountName,
  });

  String bankMasterId;
  String bankName;
  String accountNo;
  String branchName;
  String ifscNo;
  String accountName;

  factory BankDetail.fromJson(Map<String, dynamic> json) => BankDetail(
    bankMasterId: json["bank_master_id"],
    bankName: json["bank_name"],
    accountNo: json["account_no"],
    branchName: json["branch_name"],
    ifscNo: json["ifsc_no"],
    accountName: json["account_name"],
  );

  Map<String, dynamic> toJson() => {
    "bank_master_id": bankMasterId,
    "bank_name": bankName,
    "account_no": accountNo,
    "branch_name": branchName,
    "ifsc_no": ifscNo,
    "account_name": accountName,
  };
}
