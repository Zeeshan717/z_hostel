// To parse this JSON data, do
//
//     final homeModel = homeModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

HomeModel homeModelFromJson(String str) => HomeModel.fromJson(json.decode(str));

String homeModelToJson(HomeModel data) => json.encode(data.toJson());

class HomeModel {
  HomeModel({
    required this.collegeName,
    required this.location,
    required this.city,
    required this.state,
    required this.pincode,
    required this.contactNo,
    required this.alternatePhone,
    required this.emailId,
    required this.prn,
    required this.roomNo,
    required this.startDate,
    required this.endDate,
    required this.status,
    required this.insertedAt,
    required this.updatedAt,
    required this.candidateName,
    required this.applicantId,
    required this.success,
    required this.message,
  });

  String collegeName;
  String location;
  String city;
  String state;
  String pincode;
  String contactNo;
  String alternatePhone;
  String emailId;
  String prn;
  String roomNo;
  String startDate;
  String endDate;
  String status;
  String insertedAt;
  String updatedAt;
  String candidateName;
  String applicantId;
  String success;
  String message;

  factory HomeModel.fromJson(Map<String, dynamic> json) => HomeModel(
    collegeName: json["college_name"],
    location: json["location"],
    city: json["city"],
    state: json["state"],
    pincode: json["pincode"],
    contactNo: json["contact_no"],
    alternatePhone: json["alternate_phone"],
    emailId: json["email_id"],
    prn: json["prn"],
    roomNo: json["room_no"],
    startDate: json["start_date"],
    endDate: json["end_date"],
    status: json["status"],
    insertedAt: json["inserted_at"],
    updatedAt: json["updated_at"],
    candidateName: json["candidate_name"],
    applicantId: json["applicant_id"],
    success: json["success"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "college_name": collegeName,
    "location": location,
    "city": city,
    "state": state,
    "pincode": pincode,
    "contact_no": contactNo,
    "alternate_phone": alternatePhone,
    "email_id": emailId,
    "prn": prn,
    "room_no": roomNo,
    "start_date": startDate,
    "end_date": endDate,
    "status": status,
    "inserted_at": insertedAt,
    "updated_at": updatedAt,
    "candidate_name": candidateName,
    "applicant_id": applicantId,
    "success": success,
    "message": message,
  };
}
