// To parse this JSON data, do
//
//     final collegeInfoModel = collegeInfoModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

CollegeInfoModel collegeInfoModelFromJson(String str) => CollegeInfoModel.fromJson(json.decode(str));

String collegeInfoModelToJson(CollegeInfoModel data) => json.encode(data.toJson());

class CollegeInfoModel {
  CollegeInfoModel({
    required this.collegeDetail,
    required this.success,
    required this.message,
  });

  List<CollegeDetail> collegeDetail;
  String success;
  String message;

  factory CollegeInfoModel.fromJson(Map<String, dynamic> json) => CollegeInfoModel(
    collegeDetail: List<CollegeDetail>.from(json["college_detail"].map((x) => CollegeDetail.fromJson(x))),
    success: json["success"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "college_detail": List<dynamic>.from(collegeDetail.map((x) => x.toJson())),
    "success": success,
    "message": message,
  };
}

class CollegeDetail {
  CollegeDetail({
    required this.collegeId,
    required this.collegeName,
    required this.location,
    required this.city,
    required this.state,
    required this.pincode,
    required this.contactNo,
    required this.alternatePhone,
    required this.emailId,
  });

  String collegeId;
  String collegeName;
  String location;
  String city;
  String state;
  String pincode;
  String contactNo;
  String alternatePhone;
  String emailId;

  factory CollegeDetail.fromJson(Map<String, dynamic> json) => CollegeDetail(
    collegeId: json["college_id"],
    collegeName: json["college_name"],
    location: json["location"],
    city: json["city"],
    state: json["state"],
    pincode: json["pincode"],
    contactNo: json["contact_no"],
    alternatePhone: json["alternate_phone"],
    emailId: json["email_id"],
  );

  Map<String, dynamic> toJson() => {
    "college_id": collegeId,
    "college_name": collegeName,
    "location": location,
    "city": city,
    "state": state,
    "pincode": pincode,
    "contact_no": contactNo,
    "alternate_phone": alternatePhone,
    "email_id": emailId,
  };
}
