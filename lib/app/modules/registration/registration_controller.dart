import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:z_hostel/app/services/api_service.dart';

class RegistrationController extends GetxController{
  GetStorage box = GetStorage();
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();

  Future registrationApiCall(BuildContext context) async{
    String candidateId = box.read("candidate_id").toString();
    await ApiService.registrationApi(candidateId, startDateController.text, endDateController.text).then((value) {
      if(value["success"] == "1")
        {
          showDialog(
              context: context,
              builder: (BuildContext) => AlertDialog(
                title: Text("Registration Status",style: TextStyle(
                    fontSize: 12
                ),),

                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [

                    Text("Your registration has been \nsuccessfully completed!!",textAlign: TextAlign.center,)

                  ],
                ),
                actions: [
                  SizedBox(
                    height: 40,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xffee5c2b)),
                        onPressed: () {
                          Get.back();
                          Get.back();

                          // update();
                        },
                        child: Text("Cancel")),
                  )
                ],
              ));
        }else{
        showDialog(
            context: context,
            builder: (BuildContext) => AlertDialog(
              title: Text("Registration Status",style: TextStyle(
                  fontSize: 12
              ),),

              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  Text(value["details"],textAlign: TextAlign.center,)

                ],
              ),
              actions: [
                SizedBox(
                  height: 40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Color(0xffee5c2b)),
                      onPressed: () {
                        Get.back();
                        Get.back();

                        // update();
                      },
                      child: Text("Cancel")),
                )
              ],
            ));
      }
      print(value.toString());
    });
  }
}