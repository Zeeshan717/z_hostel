

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:z_hostel/app/modules/registration/registration_controller.dart';

class RegistrtionView extends StatefulWidget {
  RegistrtionView({Key? key}) : super(key: key);

  @override
  State<RegistrtionView> createState() => _RegistrionViewState();
}

class _RegistrionViewState extends State<RegistrtionView> {
 var controller = Get.put(RegistrationController());
 bool isApiRuning = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text("Registration"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(

          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
                 Text("Let's get started",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                 SizedBox(
                   height: 8,
                 ),
                 Text("Enter your start and end date",style: TextStyle(fontSize: 16,color: Colors.grey,letterSpacing: 0.5),),
            SizedBox(
              height: 20,
            ),
            TextField(
             controller:controller.startDateController,
              onTap: ()async{
                DateTime? pickedDate = await showDatePicker(

                    context: context,
                    initialDate: DateTime.now(), //get today's date
                    firstDate:DateTime(2000), //DateTime.now() - not to allow to choose before today.
                    lastDate: DateTime(2101),
                    builder: (context, child) {
                  return Theme(
                    data: Theme.of(context).copyWith(
                      colorScheme: ColorScheme.light(
                        primary: Color(0xff102038), // <-- SEE HERE
                       // onPrimary: Colors.redAccent, // <-- SEE HERE
                       // onSurface: Colors.blueAccent, // <-- SEE HERE
                      ),
                      // textButtonTheme: TextButtonThemeData(
                      //   style: TextButton.styleFrom(
                      //     primary: Colors.red, // button text color
                      //   ),
                      // ),
                    ),
                    child: child!,
                  );
                },
                );
                if(pickedDate != null ){
                  print(pickedDate);  //get the picked date in the format => 2022-07-04 00:00:00.000
                  String formattedDate = pickedDate.toString().split(" ")[0]; // format date in required form here we use yyyy-MM-dd that means time is removed
                  print(formattedDate); //formatted date output using intl package =>  2022-07-04
                  //You can format date as per your need

                  setState(() {
                    controller.startDateController.text = formattedDate; //set foratted date to TextField value.
                  });
                }else{
                  print("Date is not selected");
                }
              },
              readOnly: true,
              decoration: InputDecoration(
                hintText: "Start Date",
               prefixIcon: Icon(Icons.calendar_month,color: Colors.black,),
               border:
               OutlineInputBorder(
                 borderSide:BorderSide(
                   width:100,

                 )
               ),
                focusedBorder:OutlineInputBorder(
                    borderSide:BorderSide(
                      width:3,

                    )
                ) ,
                enabledBorder:OutlineInputBorder(
                    borderSide:BorderSide(
                      width:2,

                    )
                ) ,

                )
              ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: controller.endDateController,
              onTap: ()async{
                DateTime? pickedDate = await showDatePicker(

                  context: context,
                  initialDate: DateTime.now(), //get today's date
                  firstDate:DateTime(2000), //DateTime.now() - not to allow to choose before today.
                  lastDate: DateTime(2101),
                  builder: (context, child) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: ColorScheme.light(
                          primary: Color(0xff102038), // <-- SEE HERE
                          // onPrimary: Colors.redAccent, // <-- SEE HERE
                          // onSurface: Colors.blueAccent, // <-- SEE HERE
                        ),
                        // textButtonTheme: TextButtonThemeData(
                        //   style: TextButton.styleFrom(
                        //     primary: Colors.red, // button text color
                        //   ),
                        // ),
                      ),
                      child: child!,
                    );
                  },
                );
                if(pickedDate != null ){
                  print(pickedDate);  //get the picked date in the format => 2022-07-04 00:00:00.000
                  String formattedDate = pickedDate.toString().split(" ")[0]; // format date in required form here we use yyyy-MM-dd that means time is removed
                  print(formattedDate); //formatted date output using intl package =>  2022-07-04
                  //You can format date as per your need

                  setState(() {
                    controller.endDateController.text = formattedDate; //set foratted date to TextField value.
                  });
                }else{
                  print("Date is not selected");
                }
              },
                readOnly: true,
                decoration: InputDecoration(
                  hintText: "End Date",
                  prefixIcon: Icon(Icons.calendar_month,color: Colors.black,),
                  border:
                  OutlineInputBorder(
                      borderSide:BorderSide(
                        width:100,

                      )
                  ),
                  focusedBorder:OutlineInputBorder(
                      borderSide:BorderSide(
                        width:3,

                      )
                  ) ,
                  enabledBorder:OutlineInputBorder(
                      borderSide:BorderSide(
                        width:2,

                      )
                  ) ,

                )
            ),
            Spacer(),
            SizedBox(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: ElevatedButton(
                   style: ElevatedButton.styleFrom(backgroundColor:Color(0xff102038) ),
                  onPressed: ()async{
                     if(controller.startDateController.text.trim() == "" || controller.endDateController.text.trim() == "" ){
                       ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("All fileds required")));
                     }else{
                       setState(() {
                         isApiRuning = true;
                       });
                       await  controller.registrationApiCall(context);
                       setState(() {
                         isApiRuning = false;
                       });
                     }

                  }, child:!isApiRuning?Text(""
                  "Proceed",style: TextStyle(
                fontSize: 16
              ),):CircularProgressIndicator(
                color: Colors.white,
              )),
            ),
            SizedBox(
              height:8,
            ),
            Text("By continuing, you agree to our Terms of Service and\nPrivacy Policy",textAlign: TextAlign.center,style: TextStyle(
              color: Colors.grey, fontSize: 15
            ),)

          ],
        ),
      ),
    );
  }
}
