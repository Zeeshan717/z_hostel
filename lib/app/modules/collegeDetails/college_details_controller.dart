import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:z_hostel/app/services/api_service.dart';

import '../home/home_view.dart';

class CollegeDetailsController extends GetxController {
  GetStorage box = GetStorage();
  TextEditingController collegeNameController = TextEditingController();
  TextEditingController collegeUniqueID = TextEditingController();
  TextEditingController dobDate = TextEditingController();
  TextEditingController dobMonth = TextEditingController();
  TextEditingController dobYear = TextEditingController();

  Future loginApiCall(BuildContext context) async {
    var collegeId = box.read("collegeId");
    String dob = dobYear.text + "-" + dobMonth.text + "-" + dobDate.text;

    try{
      await ApiService.loginApi("1", " ", collegeId.toString(),
          collegeUniqueID.text.toString(), dob, " ", " ", " ", " ", " ", " ")
          .then((value) {
        print(value);
        if (value["success"].toString() == "1") {
          box.write("candidate_id", value["candidate_id"]);

          box.write("isLogin", true);
          Get.offAll(HomeView());
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(value["details"])));
        }
      });
    }catch(e){
      print(e);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Network error !")));
    }
  }
}
