import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:z_hostel/app/modules/home/home_view.dart';

import 'college_details_controller.dart';

class CollegeDetailsView extends StatefulWidget {
  CollegeDetailsView({Key? key}) : super(key: key);

  @override
  State<CollegeDetailsView> createState() => _CollegeDetailsViewState();
}

class _CollegeDetailsViewState extends State<CollegeDetailsView> {
  var controller = Get.put(CollegeDetailsController());
  @override
  void initState() {
    controller.collegeNameController.text = Get.arguments[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(
          Icons.arrow_back_sharp,
          color: Colors.black,
        ),
        title: Text(
          "College details",
          style:
              TextStyle(color: Colors.black, fontSize: 16, letterSpacing: 0.5),
        ),
        elevation: 0,
        backgroundColor: Color(0XffF1F1F5),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        color: Color(0XffF1F1F5),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Text(
              "College name",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0xff979BA3)),
                ),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * .70,
                      child: TextField(
                        controller: controller.collegeNameController,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                        cursorColor: Colors.black,
                        cursorWidth: 1.5,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: "Enter your college name",
                          hintStyle:
                              TextStyle(fontSize: 12, letterSpacing: 0.5),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Icon(
                      Icons.edit_outlined,
                      color: Color(0xff979BA3),
                    )
                  ],
                )),
            SizedBox(
              height: 20,
            ),
            Text(
              "Unique Id",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0xff979BA3)),
                ),
                child: TextField(
                  controller: controller.collegeUniqueID,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                  cursorColor: Colors.black,
                  cursorWidth: 1.5,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    hintText: "Enter your college Unique ID",
                    hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            Text(
              "Date of Birth",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Color(0xff979BA3)),
                      ),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: controller.dobDate,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                        cursorColor: Colors.black,
                        cursorWidth: 1.5,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: "Date",
                          hintStyle: TextStyle(
                            fontSize: 12,
                            letterSpacing: 0.5,
                          ),
                        ),
                      )),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Color(0xff979BA3)),
                      ),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: controller.dobMonth,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                        cursorColor: Colors.black,
                        cursorWidth: 1.5,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: "month",
                          hintStyle:
                              TextStyle(fontSize: 12, letterSpacing: 0.5),
                        ),
                      )),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Color(0xff979BA3)),
                      ),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: controller.dobYear,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                        cursorColor: Colors.black,
                        cursorWidth: 1.5,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: "Year",
                          hintStyle:
                              TextStyle(fontSize: 12, letterSpacing: 0.5),
                        ),
                      )),
                ),
              ],
            ),
            Spacer(),
            SizedBox(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Color(0xff102039)),
                    onPressed: () {
                      if (controller.collegeNameController.text.trim() == "" ||
                          controller.collegeUniqueID.text.trim() == "" ||
                          controller.dobDate.text.trim() == "" ||
                          controller.dobMonth.text.trim() == "" ||
                          controller.dobYear.text.trim() == ""
                      ){
                         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                           content: Text("all fields required !!")));
                      }else{
                        controller.loginApiCall(context);
                      }

                    },
                    child: Text(
                      "Login",
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ))),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
