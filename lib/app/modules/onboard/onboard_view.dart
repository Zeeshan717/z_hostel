

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:z_hostel/app/modules/hostelApplication/hostel_application_view.dart';
import 'package:z_hostel/app/modules/selectCollege/select_college_view.dart';

import '../../utils/color_util.dart';

class onBoardView extends StatefulWidget {
  const onBoardView({Key? key}) : super(key: key);

  @override
  State<onBoardView> createState() => _onBoardViewState();
}

class _onBoardViewState extends State<onBoardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Stack(
        children: [
          Container(
            color: ColorUtil.backgroundColor,
            padding: EdgeInsets.all(20),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Wrap(
                //alignment: WrapAlignment.spaceEvenly,
                runSpacing: 20,
                children: [
                  SizedBox(width: MediaQuery.of(context).size.width,
                    height: 40,),
                  SizedBox(width: MediaQuery.of(context).size.width,height: 10,),

                  SizedBox(width: MediaQuery.of(context).size.width,
                      child: SvgPicture.asset("assets/images/Computer.svg")),
                  SizedBox(width: MediaQuery.of(context).size.width,
                    height: 60,),

                 /* SizedBox(width: MediaQuery.of(context).size.width,
                      child: Text("Get all your college\nwork done here",style: TextStyle(color: Color(0xff102038),
                          fontSize: 36,fontWeight: FontWeight.bold),)),*/
                  SizedBox(width: MediaQuery.of(context).size.width,
                      child: Text("Hostel Needed? ",style: TextStyle(color: Color(0xff102038),
                          fontSize: 36,fontWeight: FontWeight.bold),)),

               /*   SizedBox(width: MediaQuery.of(context).size.width,
                      child: Text("Lorem ipsum tirad Oscar Jonsson, fadat \nvilopension Gosta Berg.",style: TextStyle(fontSize: 16),)),*/

                  SizedBox(width: MediaQuery.of(context).size.width,
                      child: Text("Manage it Here.",style: TextStyle(fontSize: 22),)),

                  /* SizedBox(width: MediaQuery.of(context).size.width,
                  height: 60,),*/

                ],
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            left: 20,
            child: SizedBox(width: MediaQuery.of(context).size.width-40,
                height: 60,
                child: ElevatedButton(style: ElevatedButton.styleFrom(primary: Color(0xff102038)),
                    onPressed: (){
                      Get.to( SelectCollegeView());
                    }, child: Text("Get Started"))),
          ),
        ],
      ),
    );
  }
}
