import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:z_hostel/app/services/api_service.dart';

import '../../models/bank_detatis_model.dart';
import '../../models/candidate_payment_model.dart';

class BankDetailsController extends GetxController{
  GetStorage box = GetStorage();
  List<BankDetail> bankDetailsList = [];
  Future getBankInfoApiCall() async {

    BankDetailsModel? _bankDetailsModel;
    String collegeId = box.read("collegeId").toString();
    print("--------------"+collegeId);
    await ApiService.getBankInfoApi(collegeId).then((value) {
      _bankDetailsModel = value;
      bankDetailsList = _bankDetailsModel!.bankDetail;

    });
  }

}