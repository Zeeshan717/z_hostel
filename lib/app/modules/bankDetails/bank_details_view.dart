import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../utils/color_util.dart';
import 'bank_details_controller.dart';

class BankDetailsView extends StatefulWidget {
  const BankDetailsView({Key? key}) : super(key: key);

  @override
  State<BankDetailsView> createState() => _BankDetailsState();
}

class _BankDetailsState extends State<BankDetailsView> {

  var controller = Get.put(BankDetailsController());
  void initState()  {
    controller.bankDetailsList = [];
    controller.getBankInfoApiCall().then((value) => setState((){}));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: controller.bankDetailsList.isNotEmpty?
      Container(
        color: Color(0xffeff1fe),
       // color: ColorUtil.backgroundColor,
        padding: EdgeInsets.fromLTRB(16, 30, 16, 0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child:
        SingleChildScrollView(
          child: Wrap(
            children: [
              Column(children: List.generate(controller.bankDetailsList.length, (index){
                return  Column(children: [
                  Card(
                    shadowColor:Color(0xffe3e5f2) ,
                    elevation: 20 ,
                    color: Color(0xfff8f9fe),
                    margin:EdgeInsets.only(bottom: 1),
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10))),
                  //  color:  ColorUtil.buttonColor.withOpacity(0.03),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text(controller.bankDetailsList[index].bankName),

                          SvgPicture.asset("assets/images/forwardArrow.svg"),

                        ],

                      ),
                    ),
                  ),
                  Card(
                    shadowColor:Color(0xffe3e5f2) ,
                    elevation: 20 ,
                    color: Color(0xfff8f9fe),
                    margin:EdgeInsets.only(top: 0),
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10))),
                  //  color:  ColorUtil.buttonColor.withOpacity(0.03),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 40, 20, 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Accont No.",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                              SizedBox(height: 20,),
                              Text("IFSC Code",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                              SizedBox(height: 20,),
                              Text("Branch Name",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [

                              Text(controller.bankDetailsList[index].accountNo,style: TextStyle(fontSize: 16),),
                              SizedBox(height: 20,),
                              Text(controller.bankDetailsList[index].ifscNo,style: TextStyle(fontSize: 16),),
                              SizedBox(height: 20,),
                              Text(controller.bankDetailsList[index].branchName,style: TextStyle(fontSize: 16),),
                            ],
                          ),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20,)
                ],);
              })

                ,)

            ],
          ),
        ),
      ):Center(
      child: Container(
      width: 25 ,
      height: 25,
      child: CircularProgressIndicator(
        color: Colors.black,

      ),
    ),
    ),
    );
  }
}
