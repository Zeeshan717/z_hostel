import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:z_hostel/app/services/api_service.dart';

import '../../models/home_model.dart';
import '../onboard/onboard_view.dart';
import '../selectCollege/select_college_view.dart';

class HomeController extends GetxController {
  GetStorage box = GetStorage();
  HomeModel? homeModel;
  // Logout
  Future logoutApiCall() async {
    String candidate_id = box.read("candidate_id").toString();
    await ApiService.logoutApi(candidate_id).then((value) {
      if (value["success"].toString() == "1") {
        Get.offAll(SelectCollegeView());
        box.erase();
      }
      {}
    });
  }


}
