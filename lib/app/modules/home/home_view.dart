import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:z_hostel/app/modules/paymentHistory/payment_history_view.dart';
import 'package:z_hostel/app/modules/registration/registration_view.dart';
import '../bankDetails/bank_details_view.dart';
import '../hostelApplication/hostel_application_view.dart';
import 'home_controller.dart';


class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>with TickerProviderStateMixin {
  TabController? tabController;
  var controller = Get.put(HomeController());
  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length:3);
  }
  @override
  Widget build(BuildContext context) {
    return  DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Color(0xffeff1fe),
          appBar: AppBar(

            backgroundColor: const Color(0XffF1F1F5),
            bottom: TabBar(
              unselectedLabelColor: Colors.grey,
              indicator: UnderlineTabIndicator(borderSide: BorderSide(color: Colors.black)),
              labelColor: Colors.black,
              isScrollable: true,
              indicatorColor: Colors.white,
              controller: tabController,
              tabs: const [
                Tab(
                  text: "Hostel Application",
                ),
                Tab(
                  text: "Payment History",
                ),
                Tab(
                  text: "Bank Details",
                ),

              ],
            ),
            centerTitle: true,
            title:  Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("Z Hostel",style: TextStyle(
                    color: Colors.black,
                    letterSpacing: 0.5
                ),),
              ],
            ),
          //  leading: Icon(Icons.menu,color: Colors.black,),

            actions: [
              InkWell(
                  onTap: (){
                    Get.to(RegistrtionView());
                  },
                  child: Icon(Icons.app_registration_outlined, color: Colors.black,)),
              SizedBox(
                width: 10,
              ),
              InkWell(onTap:(){
              showDialog(context: context, builder: (context)=>AlertDialog(

                title: Text("Close this app ?"),
                content: Text("Are you sure you want to logout?"),
                actions: [
                  TextButton(
                     style: TextButton.styleFrom(primary: Colors.black) ,onPressed: (){
                    controller.logoutApiCall();
                  }, child: Text("Logout")),
                  TextButton(style: TextButton.styleFrom(primary: Colors.black38),
                      onPressed: (){
                         Get.back();
                      }, child: Text("Cancel")),
                ],
              ));

              },
                child: Icon(Icons.logout, color: Colors.black,)),SizedBox(
              width: 10,
            ),

            ],

          ),
          body: TabBarView(
            controller: tabController,
            children: [HostelApplicationView(),  PaymentHistoryView(), BankDetailsView()],
          ),
        ));
    ;
  }
}
