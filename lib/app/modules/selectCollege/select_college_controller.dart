

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../models/college_info_model.dart';
import '../../services/api_service.dart';
import 'package:get_storage/get_storage.dart';

class SelectCollegeController extends GetxController{
  final box = GetStorage();
  CollegeInfoModel? _collegeInfoModel;
  TextEditingController searchController = TextEditingController();

  List<String> collageList = [];
  List<String> filterCollegeList = [];

  void searchByName () {
    filterCollegeList = [];
    collageList.forEach((element) {
      if(element.contains(searchController.text.toString()))
      {filterCollegeList.add(element);}
    });
  }
  void storeSelectedCollegeData(){
      _collegeInfoModel?.collegeDetail.forEach((element) {
        if(element.collegeName.toString() == searchController.text.toString())
          {
            box.write("college_name", element.collegeName.toString());
            box.write("collegeId", element.collegeId.toString());
          };
      });

  }

  Future getCollegeInfoCall() async{
        await ApiService.getCollegeInfo().then((value){
         _collegeInfoModel = value;
         collageList=[];
        _collegeInfoModel?.collegeDetail.forEach((element) {
         collageList.add(element.collegeName);
        });
    });
  }



}