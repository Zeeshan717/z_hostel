import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:z_hostel/app/modules/collegeDetails/college_details_view.dart';
import 'package:get/get.dart';
import 'package:z_hostel/app/modules/selectCollege/select_college_controller.dart';

class SelectCollegeView extends StatefulWidget {
  const SelectCollegeView({Key? key}) : super(key: key);

  @override
  State<SelectCollegeView> createState() => _SelectCollegeViewState();
}

class _SelectCollegeViewState extends State<SelectCollegeView> {
  var controller = Get.put(SelectCollegeController());
  @override
  void initState() {
     controller.getCollegeInfoCall().then((value)
    {
      setState(() {});
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XffF1F1F5) ,
      appBar: AppBar(
        leading:Icon(Icons.arrow_back_sharp,color: Colors.black,) ,
        title: Text("Select your College",style: TextStyle(color:Colors.black,fontSize: 16,letterSpacing: 0.5),),
        elevation: 0,
        backgroundColor: Color(0XffF1F1F5) ,) ,
      body:controller.collageList.isEmpty?
      Center(
        child: Container(
          width:25,
          height: 25,
          child:CircularProgressIndicator(
            color: Colors.black,
          ),
        ),
      ):
      Container(
        padding: EdgeInsets.symmetric(horizontal: 16,vertical: 1),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  border: Border.all(width: 1,color: Color(0xff979BA3)),
                ),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width*.70,
                      child: TextField(
                        onChanged: (value){
                          controller.searchByName();
                          setState(() {});
                        },
                        controller: controller.searchController,
                        style: TextStyle(fontSize: 12,fontWeight: FontWeight.w400),
                        cursorColor: Colors.black,
                        cursorWidth: 1.5,
                        decoration:
                        InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: "Search by college name",hintStyle: TextStyle(fontSize: 12,letterSpacing: 0.5),
                        ) ,
                      ),
                    ),
                    SizedBox(width: 8,),
                    Icon(Icons.search_sharp,color: Color(0xff979BA3),)
                  ],
                )),
            SizedBox(
              height: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: List.generate(controller.filterCollegeList.length, (index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: InkWell(
                    onTap: (){
                      controller.searchController.text = controller.filterCollegeList[index];
                      controller.filterCollegeList = [];
                      setState(() {});
                    },
                    child: Text(controller.filterCollegeList[index],style:TextStyle(fontSize: 12,fontWeight: FontWeight.w500))),
              )),
            ),
            Spacer(),
            SizedBox(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Color(0xff102039)),
                    onPressed: (){
                      controller.storeSelectedCollegeData();
                      Get.to(CollegeDetailsView(),arguments: <String>[controller.searchController.text]);

                    }, child: Text("Next",style: TextStyle(fontWeight:FontWeight.w500),))),
            SizedBox(height: 20,)
          ],
        )

        ,),
    );
  }
}
