
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:z_hostel/app/services/api_service.dart';

import '../../models/candidate_payment_model.dart';

class PaymentHistoryController extends GetxController{
  GetStorage box = GetStorage();
  List<PayDetail> payDetailsList = [];
  Future getPaymentInfoApiCall() async {

      CandidatePaymentModel? _candidatePaymentModel;
      String candidate_id = box.read("candidate_id").toString();
      String collegeId = box.read("collegeId").toString();
      print("--------------"+candidate_id);
      await ApiService.getPaymentInfoApi(candidate_id,collegeId).then((value) {
      _candidatePaymentModel = value;
      payDetailsList = _candidatePaymentModel!.payDetail;

      });
  }

}