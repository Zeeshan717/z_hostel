import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:z_hostel/app/modules/paymentHistory/payment_history_controller.dart';


import '../../utils/color_util.dart';

class PaymentHistoryView extends StatefulWidget {
  const PaymentHistoryView({Key? key}) : super(key: key);

  @override
  State<PaymentHistoryView> createState() => _PaymentHistoryState();
}

class _PaymentHistoryState extends State<PaymentHistoryView> {
  var controller = Get.put(PaymentHistoryController());
  @override
  void initState() {
    controller.payDetailsList = [];
    controller.getPaymentInfoApiCall().then((value) => setState(() {}));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: controller.payDetailsList.isNotEmpty
          ? Container(
              color: Color(0xffeff1fe),
              //  color: ColorUtil.backgroundColor,
              padding: EdgeInsets.fromLTRB(16, 30, 16, 0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Wrap(
                  children: [
                    Column(
                      children: List.generate(controller.payDetailsList.length,
                          (index) {
                        return Column(
                          children: [
                            Card(
                              shadowColor: Color(0xffe3e5f2),
                              elevation: 20,
                              color: Color(0xfff8f9fe),
                              margin: EdgeInsets.only(bottom: 1),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10))),
                              // color:  ColorUtil.buttonColor.withOpacity(0.03),
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text((index + 1).toString()),
                                    Stack(children: [
                                      SvgPicture.asset(
                                        "assets/images/RectangleGreen.svg",
                                        width: 120,
                                      ),
                                      Positioned(
                                          right: 20,
                                          top: 8,
                                          child: Text(
                                            "Success",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          )),
                                    ]),
                                  ],
                                ),
                              ),
                            ),
                            Card(
                              shadowColor: Color(0xffe3e5f2),
                              elevation: 20,
                              color: Color(0xfff8f9fe),
                              margin: EdgeInsets.only(top: 0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                              // color:  ColorUtil.buttonColor.withOpacity(0.03),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(20, 40, 20, 40),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Type",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Refund status",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Refund Transaction Id",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Refund date",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Refund amount",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Deposit paid",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Transaction date",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "Action",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Registration fees",
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          controller.payDetailsList[index]
                                                      .refundStatus ==
                                                  ""
                                              ? "N/A"
                                              : controller.payDetailsList[index]
                                                  .refundStatus,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          controller.payDetailsList[index]
                                                      .refundTxnId ==
                                                  ""
                                              ? "N/A"
                                              : controller.payDetailsList[index]
                                                  .refundTxnId,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          controller
                                              .payDetailsList[index].refundDate,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          controller.payDetailsList[index]
                                              .refundAmount,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          controller.payDetailsList[index]
                                              .paymentAmount,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "${controller.payDetailsList[index].transactionDate.toString().split(" ")[0]}, ${controller.payDetailsList[index].transactionDate.toString().split(" ")[1].split(".")[0]}",
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        SizedBox(
                                            height: 30,
                                            width: 100,
                                            child: ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .zero),
                                                    primary:
                                                        ColorUtil.buttonColor),
                                                onPressed: () async {
                                                  String url =
                                                      'https://appexperts.net/bthostel/${controller.payDetailsList[index].url}';
                                                  if (await canLaunchUrl(
                                                      Uri.parse(url))) {
                                                    await launchUrl(
                                                        Uri.parse(url),
                                                        mode: LaunchMode
                                                            .externalApplication);
                                                  } else {
                                                    throw 'Could not launch $url';
                                                  }
                                                },
                                                child: Text("Print")))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        );
                      }),
                    )
                  ],
                ),
              ),
            )
          : Center(
              child: Container(
                width: 25,
                height: 25,
                child: CircularProgressIndicator(
                  color: Colors.black,
                ),
              ),
            ),
    );
  }
}
