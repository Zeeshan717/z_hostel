

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../utils/color_util.dart';
import 'hostel_application_controller.dart';

class HostelApplicationView extends StatefulWidget {
  const HostelApplicationView({Key? key}) : super(key: key);

  @override
  State<HostelApplicationView> createState() => _HostelApplicationState();
}

class _HostelApplicationState extends State<HostelApplicationView> {
  var controller = Get.put(HostelApplicationController());
  @override
  void initState() {
    controller.geHomeInfoApiCall().then((value) => setState((){}));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:controller.homeModel!=null ?
      Container(
       // color: ColorUtil.backgroundColor,
        color: Color(0xffeff1fe),
          padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Wrap(
            children: [
              Column(children: List.generate(1, (index){
                return  Column(children: [
                  Card(
                    shadowColor:Color(0xffe3e5f2) ,
                    elevation: 20 ,
                    color: Color(0xfff8f9fe),
                    margin:EdgeInsets.only(bottom: 1),
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10))),
                 //   color:  ColorUtil.buttonColor.withOpacity(0.03),

                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text((index+1).toString()),
                          Stack(children:[
                            controller.homeModel!.status == "Complete" ? SvgPicture.asset("assets/images/RectangleGreen.svg",width: 100,):SvgPicture.asset("assets/images/rectangleRed.svg",width: 170,),
                            Positioned(
                                right: 12,
                                top: 5,
                                child: Text(controller.homeModel!.status,style: TextStyle(
                                    color: Colors.white),)),
                          ]

                          ),

                        ],

                      ),
                    ),
                  ),
                  Card(

                    shadowColor:Color(0xffe3e5f2) ,
                    elevation: 20 ,
                    margin:EdgeInsets.only(top: 0),
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10))),
                    color: Color(0xfff8f9fe),
                  //  color:  Color(0xff102038).withOpacity(0.03),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 40, 20, 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Application Id",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                              SizedBox(height: 20,),
                              Text("Applied on",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                              SizedBox(height: 20,),
                              Text("PRN",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                              SizedBox(height: 20,),
                            //  Text("Action",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [

                              Text(controller.homeModel!.applicantId,style: TextStyle(fontSize: 16),),
                              SizedBox(height: 20,),
                              Text(controller.homeModel!.insertedAt,style: TextStyle(fontSize: 16),),
                              SizedBox(height: 20,),
                              Text(controller.homeModel!.prn,style: TextStyle(fontSize: 16),),
                              SizedBox(height: 10,),
                        /*     SizedBox(
                                  height: 30,
                                  width: 100,
                                  child: ElevatedButton(

                                      style: ElevatedButton.styleFrom(
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.zero),
                                          primary: ColorUtil.buttonColor ),
                                      onPressed: (){}, child: Text("Print")))*/
                            ],
                          ),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20,)
                ],);
              })

                ,)

            ],
          ),
        ),
      ):Center(
        child: Container(
          width: 25 ,
          height: 25,
          child: CircularProgressIndicator(
            color: Colors.black,

          ),
        ),
      ),
    );
  }
}
