
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../models/home_model.dart';
import '../../services/api_service.dart';

class HostelApplicationController extends GetxController{
  GetStorage box = GetStorage();
  HomeModel? homeModel;
  // Application Details
  Future geHomeInfoApiCall() async {
    String collegeId = box.read("collegeId").toString();
    String candidateId = box.read("candidate_id").toString();

    await ApiService.geHomeInfoApi(collegeId, candidateId).then((value) {
      if (value.success.toString() == "1") {
          homeModel = value;
      }
    });
  }
}