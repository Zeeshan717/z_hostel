import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import '../models/bank_detatis_model.dart';
import '../models/candidate_payment_model.dart';
import '../models/college_info_model.dart';
import '../models/home_model.dart';

class ApiService {
  static const BaseUrl = "https://appexperts.net/bthostel/api/";

  static Future<CollegeInfoModel> getCollegeInfo() async {
    var response = await http.get(Uri.parse(
        "https://appexperts.net/bthostel/api/get_college_detail.php"));
    return collegeInfoModelFromJson(response.body);
  }

  static Future loginApi(
    String imei_no,
    String device_id,
    String college_id,
    String unique_id,
    String dob,
    String ip_address,
    String carrier_name,
    String app_version,
    String phone_model,
    String phone_manufacturer,
    String sdk_phone_version,
  ) async {
    print(dob);
    print(college_id);
    print(unique_id);
    final response =
        await http.post(Uri.parse("${BaseUrl}user_login.php"), body: {
      "imei_no": imei_no,
      "device_id": device_id,
      "college_id": college_id,
      "unique_id": unique_id,
      "dob": dob,
      "ip_address": ip_address,
      "carrier_name": carrier_name,
      "app_version": app_version,
      "phone_model": phone_model,
      "phone_manufacturer": phone_manufacturer,
      "sdk_phone_version": sdk_phone_version,
    });

    print("Login Api Data" + response.body);

    return jsonDecode(response.body);
  }

  static Future<HomeModel> geHomeInfoApi(candidate_id, college_id) async {
    var response = await http.post(Uri.parse("${BaseUrl}get_home.php"),
        body: {'candidate_id': candidate_id, 'college_id': college_id});
    return homeModelFromJson(response.body);
  }

  static Future<BankDetailsModel> getBankInfoApi(college_id) async {
    var response = await http.post(Uri.parse("${BaseUrl}get_bank_master_detail.php"),
        body: {'college_id': college_id});
    return bankDetailsModelFromJson(response.body);
  }

  static Future<CandidatePaymentModel> getPaymentInfoApi(candidate_id, college_id) async {
    var response = await http.post(Uri.parse("${BaseUrl}get_candidate_payment_detail.php"),
        body: {
      'candidate_id': candidate_id,
      'college_id': college_id,

        });
    return candidatePaymentModelFromJson(response.body);

  }

  static Future logoutApi(candidate_id) async {
    var response = await http.post(Uri.parse("${BaseUrl}user-logout.php"),
        body: {'candidate_id': candidate_id,});
    print(response.body.toString()+"-----logout Data-------");
    return jsonDecode(response.body);

  }
  static Future registrationApi(candidate_id,startDate, endDate) async {
    print(candidate_id.toString());
    print(startDate.toString());
    print(endDate.toString());
    print("working");
    try{
      var response = await http.post(Uri.parse("${BaseUrl}apply_hostel_Application.php"),
          body: {
            'candidate_id':candidate_id,
            'startdate': startDate,
            'enddate' : endDate
          });
      print(response.body.toString()+"-----logout Data-------");
      return jsonDecode(response.body);
    }catch(e){
      print("Not working");
    }


  }

//   static Future registrationApi(
//       String usertype,
//       String insname,
//       String asname,
//       String email,
//       String phoneno,
//       String address,
//       String studentno,
//       String sname,
//       String phone,
//       String stuemail,
//       String parentname,
//       String dob,
//       String purpose,
//       String statedetail,
//       String about_yourself,
//       String disablility,
//       String medical_issue,
//       String assistance_required,
//       String know_about,
//       String talent_show,
//       String talent_show_detail,
//       File aadharFileName,
//       File uuidFileName,
//       ) async {
//
//       // print('${usertype}');
//       // print('insname ${insname}');
//       // print('asname ${asname}');
//       // print('email ${email}');
//       // print('phoneno ${phoneno}');
//       // print('address ${address}');
//       // print('studentno ${studentno}');
//       // print('sname ${sname}');
//       // print('${phone}');
//       // print('${stuemail}');
//       // print('${parentname}');
//       // print('${dob}');
//       // print('${purpose}');
//       // print('${statedetail}');
//       // print('${about_yourself}');
//       // print('${disablility}');
//       // print('${medical_issue}');
//       // print('${assistance_required}');
//       // print('${know_about}');
//       // print('${talent_show}');
//       // print('${talent_show_detail}');
//       // print('${aadharFileName}');
//       // print('${uuidFileName}');
//
//     var postUri = Uri.parse(
//         "${BaseUrl}registration");
//     var request = http.MultipartRequest("POST", postUri);
//     request.fields['usertype'] = usertype;
//     request.fields['insname'] = insname;
//     request.fields['asname'] = asname;
//     request.fields['email'] = email;
//     request.fields['phoneno'] = phoneno;
//     request.fields['address'] = address;
//     request.fields['studentno'] = studentno;
//     request.fields['sname'] = sname;
//     request.fields['phone'] = phone;
//     request.fields['stuemail'] = stuemail;
//     request.fields['parentname'] = parentname;
//     request.fields['dob'] = dob;
//
//     request.fields['purpose'] = purpose;
//     request.fields['statedetail'] = statedetail;
//     request.fields['about_yourself'] = about_yourself;
//     request.fields['disablility'] = disablility;
//     request.fields['medical_issue'] = medical_issue;
//     request.fields['assistance_required'] = assistance_required;
//     request.fields['know_about'] = know_about;
//     request.fields['talent_show'] = talent_show;
//     request.fields['talent_show_detail'] = talent_show_detail;
//
//     try {
//       request.files.add(
//           await http.MultipartFile.fromPath("upload_dob_certificate", aadharFileName.path));
//     } catch (e) {
//       print(e.toString());
//
//       // request.fields['upload_dob_certificate'] = "";
//     }
//     try {
//       request.files.add(
//           await http.MultipartFile.fromPath("upload_certificate", uuidFileName.path));
//     } catch (e) {
//       print(e.toString());
//         // request.fields['upload_dob_certificate'] = "";
//     }
//
//     http.Response response =
//     await http.Response.fromStream(await request.send());
//
//     return jsonDecode(response.body);
//
//   }
//
//   static Future registrationStudentApi(
//
//       String sname,
//       String phone,
//       String email,
//       String parentname,
//       String dob,
//       String Institution_id,
//       String purpose,
//       String statedetail,
//       String about_yourself,
//       String disablility,
//       String medical_issue,
//       String assistance_required,
//       String know_about,
//       String talent_show,
//       String talent_show_detail,
//       File aadharFileName,
//       File uuidFileName,
//       ) async {
//   print("--------");
//     var postUri = Uri.parse(
//         "${BaseUrl}registration_student");
//     var request =  http.MultipartRequest("POST", postUri);
//     request.fields['sname'] = sname;
//     request.fields['phone'] = phone;
//     request.fields['email'] = email;
//     request.fields['parentname'] = parentname;
//     request.fields['dob'] = dob;
//    try{
//      request.fields['Institution_id'] = Institution_id;
//    }
//    catch(e){
//     print('00000000');
//    }
//     request.fields['purpose'] = purpose;
//     request.fields['statedetail'] = statedetail;
//     request.fields['about_yourself'] = about_yourself;
//     request.fields['disablility'] = disablility;
//     request.fields['medical_issue'] = medical_issue;
//     request.fields['assistance_required'] = assistance_required;
//     request.fields['know_about'] = know_about;
//     request.fields['talent_show'] = talent_show;
//     request.fields['talent_show_detail'] = talent_show_detail;
//
//   try {
//     request.files.add(
//         await http.MultipartFile.fromPath("upload_dob_certificate", aadharFileName.path));
//   } catch (e) {
//     print(e.toString());
//
//     // request.fields['upload_dob_certificate'] = "";
//   }
//   try {
//     request.files.add(
//         await http.MultipartFile.fromPath("upload_certificate", uuidFileName.path));
//   } catch (e) {
//     print(e.toString());
//     // request.fields['upload_dob_certificate'] = "";
//   }
//
//   http.Response response =
//   await http.Response.fromStream(await request.send());
//
//   print("-------"+response.toString());
//
//   return jsonDecode(response.body);
// }
//
//   // static Future<> getParticipantData(String institutionId) async {
//   //   final response = await http.post(Uri.parse("${BaseUrl}get_institute_detail"),
//   //       body: {"Institution_id": institutionId,});
//   //   if (response.statusCode == 200) {
//   //     return ppfAccountModelFromJson(response.body);
//   //   }
//   //   return ppfAccountModelFromJson(response.body);
//   // }

}
