import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:z_hostel/app/modules/onboard/onboard_view.dart';
import 'package:get_storage/get_storage.dart';

import 'app/modules/home/home_view.dart';

void main() async{
  await GetStorage.init();
  runApp( MyApp());
}

class MyApp extends StatelessWidget {

  MyApp({Key? key}) : super(key: key);
  GetStorage box = GetStorage();


  @override
  Widget build(BuildContext context) {
    bool isloggedin = box.read("isLogin")??false;
    return GetMaterialApp(
        home:isloggedin?HomeView():onBoardView(),
    );

  }
}
